# Test timonel

Multiple choice test para el examen de timonel:
[Play](https://santiher.gitlab.io/timonel_quizz/)

## To do

Add questions for:

* [ ] Ejercicios de calculo de mareas.
* [ ] Maniobras.
* [ ] Motor.
* [ ] Navegación con mal tiempo.
* [ ] Seguridad.
* [ ] Tipos de navegacion: ceñida, descuartelar, travez, etc.
