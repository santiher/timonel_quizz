const CACHE_NAME = 'static-cache-v1';
const SUBDOMAIN = '/timonel_quizz';
const FILES_TO_CACHE = [
    SUBDOMAIN + '/index.html',
    SUBDOMAIN + '/css/bootstrap.min.css',
    SUBDOMAIN + '/css/bootstrap.min.css.map',
    SUBDOMAIN + '/css/buoys.css',
    SUBDOMAIN + '/css/style.css',
    SUBDOMAIN + '/js/player.js',
    SUBDOMAIN + '/js/questions.js',
    SUBDOMAIN + '/images/anchor_admiralty.jpg',
    SUBDOMAIN + '/images/anchor_danforth.jpg',
    SUBDOMAIN + '/images/barco01.png',
    SUBDOMAIN + '/images/barco02.jpg',
    SUBDOMAIN + '/images/barco03.png',
    SUBDOMAIN + '/images/barco04.jpg',
    SUBDOMAIN + '/images/beaufort.png',
    SUBDOMAIN + '/images/buoy01.png',
    SUBDOMAIN + '/images/buoy02.png',
    SUBDOMAIN + '/images/buoy04.png',
    SUBDOMAIN + '/images/buoy05.png',
    SUBDOMAIN + '/images/buoy06.png',
    SUBDOMAIN + '/images/buoy07.png',
    SUBDOMAIN + '/images/buoy08.png',
    SUBDOMAIN + '/images/buoy09.png',
    SUBDOMAIN + '/images/buoy10.png',
    SUBDOMAIN + '/images/buoy11.png',
    SUBDOMAIN + '/images/buoy12.png',
    SUBDOMAIN + '/images/buoy13.png',
    SUBDOMAIN + '/images/buoy14.png',
    SUBDOMAIN + '/images/caladoaereo.png',
    SUBDOMAIN + '/images/logo_boat_128x128.png',
    SUBDOMAIN + '/images/logo_boat_144x144.png',
    SUBDOMAIN + '/images/logo_boat_152x152.png',
    SUBDOMAIN + '/images/logo_boat_192x192.png',
    SUBDOMAIN + '/images/logo_boat_256x256.png',
    SUBDOMAIN + '/images/logo_boat_512x512.png',
    SUBDOMAIN + '/images/logo_boat_900x900.png',
    SUBDOMAIN + '/images/logo_boat_96x96.png',
    SUBDOMAIN + '/images/luces_01.png',
    SUBDOMAIN + '/images/luces_02.png',
    SUBDOMAIN + '/images/luces_03.png',
    SUBDOMAIN + '/images/luces_04.png',
    SUBDOMAIN + '/images/luces_05.png',
    SUBDOMAIN + '/images/luces_06.png',
    SUBDOMAIN + '/images/luces_07.png',
    SUBDOMAIN + '/images/luces_08.png',
    SUBDOMAIN + '/images/luces_09.png',
    SUBDOMAIN + '/images/luces_angulos.png',
    SUBDOMAIN + '/images/mareas0.png',
    SUBDOMAIN + '/images/nombre_cabos_amarra.png',
    SUBDOMAIN + '/images/ripa_buque_que_alcanza.png',
    SUBDOMAIN + '/images/ripa_buque_que_alcanza_solucion.png',
    SUBDOMAIN + '/images/ripa_cruce_motor.png',
    SUBDOMAIN + '/images/ripa_cruce_motor_solucion.png',
    SUBDOMAIN + '/images/ripa_distintas_amuras.png',
    SUBDOMAIN + '/images/ripa_distintas_amuras_solucion.png',
    SUBDOMAIN + '/images/ripa_iguales_amuras_2.png',
    SUBDOMAIN + '/images/ripa_iguales_amuras_2_solucion.png',
    SUBDOMAIN + '/images/ripa_iguales_amuras.png',
    SUBDOMAIN + '/images/ripa_iguales_amuras_solucion.png',
    SUBDOMAIN + '/images/ripa_varios.png',
    SUBDOMAIN + '/images/ripa_vuelta_encontrada.png',
    SUBDOMAIN + '/images/ripa_vuelta_encontrada_solucion.png',
];

// Precache static resources here.
self.addEventListener('install', (evt) => {
  console.log('[ServiceWorker] Install');
  evt.waitUntil(
      caches.open(CACHE_NAME).then((cache) => {
        console.log('[ServiceWorker] Pre-caching offline page');
        return cache.addAll(FILES_TO_CACHE);
      })
  );
  self.skipWaiting();
});

// Remove previous cached data from disk.
self.addEventListener('activate', (evt) => {
  console.log('[ServiceWorker] Activate');
  evt.waitUntil(
      caches.keys().then((keyList) => {
        return Promise.all(keyList.map((key) => {
          if (key !== CACHE_NAME) {
            console.log('[ServiceWorker] Removing old cache', key);
            return caches.delete(key);
          }
        }));
      })
  );
  self.clients.claim();
});

// Add fetch event handler here for caching.
self.addEventListener('fetch', function(evt) {
  console.log('[ServiceWorker] Fetch url: ', evt.request.url);
  var resource;
  if (evt.request.url.includes('/images/') ||
      evt.request.url.includes('/css/') ||
      evt.request.url.includes('/js/')) {
    var dir_file = evt.request.url.split('/').slice(-2);
    resource = SUBDOMAIN + '/' + dir_file.join('/');
  } else if (evt.request.url.includes('/index.html')) {
    resource = SUBDOMAIN + '/index.html';
  }
  console.log('[ServiceWorker] Fetch resource: ', resource || 'home');
  if (resource != undefined) {
    evt.respondWith(
      caches.match(resource).then(
        function(response) {
          return response || fetch(evt.request);
        }
      )
    );
  } else {
    evt.respondWith(
      fetch(evt.request)
    )
  }
});
