var amount = 'loop'
var current = 0
var correct = 0
var correct_last_ten = []
var questions = []
var questions_full = []
var question_answer = 0
var topics = []

function start() {
    var inputs = document.getElementById("setup").getElementsByTagName("input")
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].checked) {
            var topic = inputs[i].id.slice(6)
            topics.push(topic)
        }
    }
    // NOTE: all_questions is defined in other file and must be imported
    for (var i = 0; i < all_questions.length; i++) {
        if (topics.indexOf(all_questions[i].topic) >= 0) {
            questions.push(all_questions[i])
            questions_full.push(all_questions[i])
        }
    }
    document.getElementById("setup").style.display = "None"
    document.getElementById("scoring").style.display = ""
    document.getElementById("test").style.display = ""
    amount = document.querySelector(
        'input[name="amount_of_questions"]:checked').value
    if (amount_is_number()) {
        amount = parseInt(amount)
    }
    if (questions.length > 0) {
        next_question()
    }
}

function draw_scoring() {
    if (current > 0) {
        correct_text = ": " + Math.round(100 * correct / current) + "%"
        last_ten_text = (
            ": "
            + Math.round(100 * correct_last_ten.reduce((a,b) => a + b, 0)
                         / Math.min(current, 10))
            + "%")
    } else {
        correct_text = ": -"
        last_ten_text = ": -"
    }
    document.getElementById("score_correct").innerHTML = correct_text
    document.getElementById("score_last_ten").innerHTML = last_ten_text
    document.getElementById("score_total").innerHTML = (
        ": " + correct + "/" + current)
}

function shuffle(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array
}

function next_question() {
    document.getElementById("question_next").style.display = "None"
    if ((amount == 'all' && questions.length <= 0) ||
        (amount_is_number() && current >= amount)) {
        document.getElementById("test").style.display = "None"
        return
    }
    current += 1
    // Get random question avoiding repetition for as long as possible
    if (questions.length <= 0) {
        for (var i = 0; i < questions_full.length; i++) {
            questions.push(questions_full[i])
        }
    }
    var question_number = Math.floor(Math.random() * questions.length)
    var question = questions.splice(question_number, 1)[0];
    // Write question
    document.getElementById("question").innerHTML = question.question
    // Draw media
    if (question.media != undefined) {
        document.getElementById("question_media").innerHTML = question.media
    } else {
        document.getElementById("question_media").innerHTML = ""
    }
    // Give shuffled options and keep correct answer
    var answer = question.options[question.answer]
    var options = shuffle(question.options.slice())
    var options_div = document.getElementById("question_answers")
    var question_answers_html = ""
    for (var i = 0; i < options.length; i++) {
        if (options[i] == answer) {
            question_answer = i
        }
        question_answers_html += (
            '<div class="col" style="cursor: pointer" onclick=answer('
            + i
            + ')>'
            + options[i]
            + '</div>'
        )
    }
    options_div.innerHTML = question_answers_html
    // Set invisible explanation
    var explanation_div = document.getElementById("question_explanation")
    explanation_div.style.display = "None"
    if (question.explanation != undefined) {
        explanation_div.innerHTML = question.explanation
    } else {
        explanation_div.innerHTML = ""
    }
}

function answer(selected) {
    // Update correct metrics
    if (selected == question_answer) {
        correct += 1
        correct_last_ten.push(1)
    } else {
        correct_last_ten.push(0)
    }
    correct_last_ten = correct_last_ten.slice(
        Math.max(correct_last_ten.length - 10, 0))
    draw_scoring()
    // Remove function from answer options
    var options = (document.getElementById("question_answers")
        .getElementsByTagName("div"))
    for (var i = 0; i < options.length; i++) {
        if (i == selected && selected != question_answer) {
            options[i].style.backgroundColor = "#FF456D"
            options[i].style.borderColor = "#D93B5D"
        }
        if (i == question_answer) {
            options[i].style.backgroundColor = "#6DFF45"
            options[i].style.borderColor = "#5DD93A"
        }
        options[i].removeAttribute("onclick")
    }
    // Display next question button
    document.getElementById("question_next").style.display = ""
    // Display explanation
    document.getElementById("question_explanation").style.display = ""
}

function amount_is_number() {
    return amount != 'all' && amount != 'loop'
}

function start_all_questions() {
    var inputs = document.getElementById("setup").getElementsByTagName("input")
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].checked) {
            var topic = inputs[i].id.slice(6)
            topics.push(topic)
        }
    }
    // NOTE: all_questions is defined in other file and must be imported
    for (var i = 0; i < all_questions.length; i++) {
        if (topics.indexOf(all_questions[i].topic) >= 0) {
            questions.push(all_questions[i])
            questions_full.push(all_questions[i])
        }
    }
    document.getElementById("setup").style.display = "None"
    // Draw questions
    questions_div = document.getElementById("questions")
    for (var q = 0; q < questions.length; q++) {
        var question = questions[q]
        // Write question
        question_html  = '      <hr>'
        question_html += '      <div id="test">'
        question_html += '        <div id="question">'
        question_html +=            question.question
        question_html += '        </div>'
        question_html += '        <div id="question_media">'
        question_html +=            (question.media || '')
        question_html += '        </div>'
        question_html += '        <div id="question_answers" class="row">'
        question_html += '          <div class="col">'
        question_html +=              question.options[question.answer]
        question_html += '          </div>'
        question_html += '        </div>'
        question_html += '        <div id="question_explanation">'
        question_html +=            (question.explanation || '')
        question_html += '        </div>'
        question_html += '      <div>'
        questions_div.innerHTML += question_html
    }
}
