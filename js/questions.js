all_questions = [
    {
        topic: "general",
        kind: "text",
        question: "¿En que barco sale Gabutti?",
        options: ["Rante", "Soledad", "Malvina"],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Cuántas anclas tiene un barco?",
        options: ["1", "2", "3", "11"],
        explanation: 'Por eso los marineros dicen "eleven anclas".',
        answer: 3,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Se puede ir en contra del viento?",
        options: ["Si", "No"],
        answer: 1,
        explanation: "A motor no cuenta ❤️"
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Cuales son los 4 problemas de la navegación?",
        options: [
            "Dirección, Distancia, Posición, Profundidad",
            "Dirección, Posición, Profundidad, Velocidad",
            "Distancia, Posición, Profundidad, Velocidad",
            "Distancia, Profundidad, Mareas, Velocidad",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "Qué forma tiene la Tierra?",
        options: [
            "Geoide",
            "Esfera",
            "Cono",
            "Cuadrado",
            "Circulo",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Con qué letra griega se designa la Latitud?",
        options: [
            "φ (phi)",
            "ω (omega)",
            "θ (theta)",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Con qué letra griega se designa la Longitud?",
        options: [
            "ω (omega)",
            "φ (phi)",
            "ψ (psi)",
            "λ (lambda)",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Qué es la Latitud?",
        options: [
            "La distancia angular que parte del ecuador hacia el norte o sur, de 0° a 90° hasta el paralelo del observador",
            "La distancia angular que parte del meridiano 0 hacia el oeste o este, de 0° a 180° hasta el meridiano del observador",
            "Ninguna de las anteriores",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Qué es la Longitud?",
        options: [
            "La distancia angular que parte del meridiano 0 hacia el oeste o este, de 0° a 180° hasta el meridiano del observador",
            "La distancia angular que parte del ecuador hacia el norte o sur, de 0° a 90° hasta el paralelo del observador",
            "Ninguna de las anteriores",
        ],
        answer: 0
    },
        {
        topic: "charts",
        kind: "text",
        question: "¿En que latitud se encuentra el Ecuador?",
        options: [
            "0° 0' 0''",
            "23º 26′ 14'' S",
            "66º 33' 52'' N",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿En que latitud se encuentra el Circulo Polar Artico?",
        options: [
            "66º 33' 52'' N",
            "23º 26′ 14'' S",
            "0° 0' 0''",
            "66º 33' 52'' S",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿En que latitud se encuentra el Trópico de Capricornio?",
        options: [
            "23º 26′ 14'' S",
            "66º 33' 52'' N",
            "66º 33' 52'' S",
        ],
        answer: 0
    },
    {
        topic: "general",
        kind: "text",
        question: "Seleccione 4 formas de navegación",
        options: [
            "A vista de costa, por estima, astronómica, por GPS",
            "Por estima, por GPS, astronómica, a vista de sonsos",
            "A vista de costa, astronómica, a vista de sonsos",
            "Todas las anteriores",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Cómo se llama a la distancia más corta entre dos puntos de la superficie terrestre que se mide a lo largo del arco de un círculo máximo que pasa por ambos?",
        options: [
            "Ortodromia",
            "Loxodromia",
            "Caminito",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Que es una Loxodromia?",
        options: [
            "Es el camino recorrido por un barco que siempre se lo navega a rumbo constante. Exceptuando los casos particulares (Rumbos 000°, 180°, 090° y 270°) cualquier sea el rumbo al que se gobierna, el buque llegaría indefectiblemente a uno de los polos",
            "Es la distancia más corta entre dos puntos y se mide a lo largo del arco de un círculo máximo que pasa por ambos",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Cómo se clasifican las cartas nauticas según su extensión?",
        options: [
            "Cartas generales, cartas particulares, cuarterones",
            "Cartas generales, cartas particulares, cuarterones, cartas oceanicas, cartas de ruta, cartas de recalada, cartas costeras",
            "Todas son correctas",
        ],
        explanation: "Clasicamente se contaba con la división en tres tipos. Desde que se creó la Organización Hidrográfica Internacional se amplió la clasificación.",
        answer: 2
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Cómo se dibujan las ortodromias en una carta Mercator?",
        options: [
            "Como curvas con la concavidad mirando al ecuador",
            "Como rectas",
            "Como lineas en zig zag",
            "Ninguna de las anteriores",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Cómo se dibujan las loxodromias en una carta Mercator?",
        options: [
            "Como rectas",
            "Como curvas con la concavidad mirando al ecuador",
            "Como lineas en zig zag",
            "Ninguna de las anteriores",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Cómo se dibujan las ortodromias en una carta Gnomónica?",
        options: [
            "Como rectas",
            "Como curvas con la concavidad mirando al ecuador",
            "Como lineas en zig zag",
            "Ninguna de las anteriores",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Qué requisitos debe tener una carta náutica?",
        options: [
            "Plana, Isogónica, Conforme, Que las loxodromias se representen como rectas, A escala, Actualizables",
            "Plana, Isogónica, Conforme, Que las loxodromias se representen como rectas, A escala",
            "Plana, Isogónica, Conforme, Que las ortodromias se representen como rectas, A escala, Actualizables",
            "Plana, Isogónica, Comoda, Que las loxodromias se representen como rectas, A escala, Actualizables",
            "Plana, Comoda, Que las loxodromias se representen como rectas, A escala, Actualizables",
            "Esferica, Isogónica, Conforme, Que las ortodromias se representen como rectas, A escala",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Hasta que latitud son utiles las cartas de tipo Mercator?",
        options: [
            "60°",
            "30°",
            "90°",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Qué norte se muestra en una carta náutica?",
        options: [
            "El verdadero",
            "El magnetico",
            "El norte del compas",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Cuantos nortes hay?",
        options: [
            "3",
            "1",
            "2",
            "4",
            "5",
        ],
        explanation: "Estan el norte verdadero, norte magnetico y norte del compas.",
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Qué es el rumbo?",
        options: [
            "Es el ángulo horizontal que parte desde el norte en sentido horario de 0° a 360° hasta la linea de crujia del barco",
            "Otra cosa",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Dónde busco los símbolos o signos que figuran en la carta y no conozco?",
        options: [
            "Todas son válidas",
            "En la H-5000",
            "En google",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Cada cuánto se publican los avisos a los navegantes",
        options: [
            "Mensualmente",
            "Semanalmente",
            "Diariamente",
            "Anualmente",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Qué publicación náutica utilizo para actualizar las cartas?",
        options: [
            "Avisos a los navegantes",
            "Oreja de burro",
            "Ninguno",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Cada cuanto se actualiza la declinación magnetica?",
        options: [
            "Anualmente",
            "Mensualmente",
            "Semanalmente",
            "Diariamente",
        ],
        answer: 0
    },
    {
        topic: "charts",
        kind: "text",
        question: "¿Donde se busca la declinación magnetica?",
        options: [
            "En la rosa de los vientos de la carta náutica más cercana",
            "En Bing",
            "No se busca, se crea",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Donde busco el desvio del compas (δ)?",
        options: [
            "En la Tabla de desvió de compas remanente, la cual es propia de cada embarcación",
            "En la carta náutica",
            "En yahoo answers",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Cómo es la formula del Rumbo verdadero (Rv)?",
        options: [
            "Rv = Rc + Dm + δ",
            "?",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Cómo es la formula del Rumbo compas (Rc)?",
        options: [
            "Rc = Rv - Dm - δ",
            "?",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "Dado Rv=090° y Dm=9°W ¿Cual es Rm?",
        options: [
            "099°",
            "081°",
            "090°",
        ],
        explanation: "Rm = Rv – Dm",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "Dado Rv=300° y Dm=20°W ¿Cual es Rm?",
        options: [
            "320°",
            "280°",
            "300°",
            "230°",
        ],
        explanation: "Rm = Rv – Dm",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "Dado Rv=045°, Dm=24°E y δ=-5° ¿Cual es Rc?",
        options: [
            "026°",
            "064°",
            "074°",
            "230°",
        ],
        explanation: "Rc = Rv – Dm – δ. <br>Rc = 045° - (+24°) – (-5°) = 026",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Qué es una milla náutica?",
        options: [
            "Es un minuto de arco de circulo máximo",
            "1200Km",
            "Otra cosa",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿A cuantos metros equivale una milla marina?",
        options: [
            "1852 metros",
            "1200 metros",
            "1582 metros",
            "1000 metros",
            "Ninguna de las anteriores",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Cuántas millas Marinas tengo que hacer para darle la vuelta al mundo por el Ecuador?",
        options: [
            "21600 millas",
            "12000 millas",
            "3600 millas",
            "32400 millas",
        ],
        explanation: "El ecuador es un circulo máximo. <br>Tiene 360° grados. <br>Una milla es 1' de arco de circulo máximo.<br>En 1°  hay 60'. <br>Entonces 360° x 60 = 21600 millas",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Qué es un nudo?",
        options: [
            "1 milla / hora",
            "1 km / hora",
            "No se",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Con que mido la velocidad de propulsión en una embarcación?",
        options: [
            "Corredera",
            "GPS",
            "VHF",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Con que mido la velocidad efectiva en una embarcación?",
        options: [
            "GPS",
            "Corredera",
            "VHF",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "Si navego a una velocidad de 6 nudos por 2 horas y 40 minutos. ¿Cuánta distancia navegue?",
        options: [
            "16 millas",
            "12 millas",
            "20 millas",
        ],
        explanation: "1 nudo = 1 milla / hora.<br>40 minutos = 40 / 60 horas. <br>2 horas y 40 minutos = 2.66 horas. <br>(6 millas / hora) * (2.66 horas) = 16 millas",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "Avancé 45 millas en 90 minutos ¿A qué velocidad viaje?",
        options: [
            "30 nudos",
            "20 nudos",
            "40 nudos",
            "1 Km/hora",
        ],
        explanation: "Velocidad = Distancia / Tiempo.<br>1 nudo = 1 milla / hora.<br>1 hora = 60 minutos.<br>90 minutos = 1.5 horas.<br>Velocidad = 45 millas / 90 minutos.<br>Velocidad = 45 millas / 1.5 horas.<br>Velocidad = (45 millas / 1.5) * (millas / horas).<br>Velocidad = 30 millas / hora.<br>Velocidad = 30 nudos",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Cual es la formula de la velocidad?",
        options: [
            "velocidad = distancia / tiempo",
            "velocidad = tiempo / distancia",
            "velocidad = nudos / millas",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "Dm=10° 14' W 2005 (8' E) ¿Cuanto vale Dm en 2010-02-12?",
        options: [
            "9° 34' W. Usamos 10° W ",
            "9° 34' W. Usamos 9° W ",
            "9° 44' W. Usamos 10° W ",
            "9° 44' W. Usamos 9° W ",
        ],
        explanation: "De enero 2005 a Octubre 2010 hay 5 años.<br>5 * 8' E = 40' E.<br>10° 14' W + 40' E = 9° 34' W.<br>Redondeamos para arriba (34 > 30, 30 es la mitad de 60).",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "Dm=0° 05' E 2008 (9' W) ¿Cuanto vale Dm en 2016-10-20?",
        options: [
            "1° W",
            "1° E",
            "0°",
            "1° 05' E",
        ],
        explanation: "De enero 2008 a Octubre 2016 hay 8.8 años, como pasamos junio usamos 9.<br>9 * 9' W = 81' = 60' + 21' = 1° 21' W.<br>0° 05' E + 1° 21' W = 1° W.",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Dónde se miden las distancias en la carta náutica?",
        options: [
            "En la escala de latitudes",
            "En la escala de longitudes",
            "En cualquiera",
        ],
        explanation: "La escala de longitudes no es constante. La de latitudes si.",
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Qué es una marcación?",
        options: [
            "El ángulo horizontal que parte desde el norte de 0° a 360° hasta la visual a un punto notable.",
            "Un punto reconocible marcado en una carta náutica",
            "Otro",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Qué es una linea de posición?",
        options: [
            "Una linea sobre la que me encuentro",
            "?",
        ],
        answer: 0
    },
    {
        topic: "course",
        kind: "text",
        question: "¿Cómo resolves un ejercicio de la carta náutica?",
        options: [
            "1. Ubicas los puntos.<br>2. Dibujas una linea entre los puntos.<br>3. Medis el ángulo entre el norte y el rumbo (la linea dibujada).<br>4. Rm = Rv - Dm (Dm en la rosa de los vientos).<br>5. Rc = Rm - δ (δ es un dato).<br>6. Distancia: medis la linea y la medis en la escala de latitudes.",
            "Como yo quiero 😡",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy01.png">',
        options: [
            "Peligro Aislado",
            "Aguas seguras",
            "Cardinal este",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy02.png">',
        options: [
            "Canal preferido a estribor, región B",
            "Canal preferido a babor, región B",
            "Cardinal oeste",
            "Marca de estribor de veril de canal, región B",
        ],
        explanation: "Cuando un canal se bifurca, se marca el canal preferido con esta boya.",
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy04.png">',
        options: [
            "Canal preferido a babor, región B",
            "Canal preferido a estribor, región B",
            "Cardinal oeste",
            "Marca de babor de veril de canal, región A",
        ],
        explanation: "Cuando un canal se bifurca, se marca el canal preferido con esta boya.",
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy05.png">',
        options: [
            "Aguas seguras",
            "Peligro aislado",
            "Señal especial",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy06.png">',
        options: [
            "Señal especial",
            "Aguas seguras",
            "Peligro aislado",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy07.png">',
        options: [
            "Nuevo hundimiento",
            "Señal especial",
            "Aguas seguras",
            "Peligro aislado",
        ],
        explanation: "Poco frecuentes en Argentina. Es importante notar que rompen la convención de lineas verticales seguro, lineas horizontales peligro.",
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boyas son estas?",
        media: '<img src="images/buoy08.png">',
        options: [
            "Cardinales",
            "Veriles de canal",
            "Aguas seguras",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy09.png">',
        options: [
            "Cardinal norte",
            "Cardinal este",
            "Cardinal sur",
            "Cardinal oeste",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy10.png">',
        options: [
            "Cardinal norte",
            "Cardinal este",
            "Cardinal sur",
            "Cardinal oeste",
        ],
        answer: 1
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy11.png">',
        options: [
            "Cardinal norte",
            "Cardinal este",
            "Cardinal sur",
            "Cardinal oeste",
        ],
        answer: 2
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy12.png">',
        options: [
            "Cardinal norte",
            "Cardinal este",
            "Cardinal sur",
            "Cardinal oeste",
        ],
        answer: 3
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy13.png">',
        options: [
            "Marca de estribor en la región B ingresando desde el mar",
            "Marca de babor en la región B ingresando desde el mar",
            "Otra",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya es esta?",
        media: '<img src="images/buoy14.png">',
        options: [
            "Marca de babor en la región B ingresando desde el mar",
            "Marca de estribor en la región B ingresando desde el mar",
            "Otra",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué indica una cardinal norte?",
        options: [
            "Hay que pasar la boya por el norte",
            "Hay que pasar la boya por cualquier lado menos por el norte",
            "Ninguna de las anteriores",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que luces emite una boya de aguas seguras?",
        options: [
            "Luz blanca con 1 destello corto y 1 largo (+ descanso)",
            "Luz blanca con 2 destellos cortos (+ descanso)",
            "Luz blanca con destellos cortos permanentes",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que luces emite una boya de peligro aislado?",
        options: [
            "Luz blanca con 2 destellos cortos (+ descanso)",
            "Luz blanca con 1 destello corto y 1 largo (+ descanso)",
            "Luz blanca con destellos cortos permanentes",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que luces emite una boya cardinal norte?",
        options: [
            "Luz blanca con destellos cortos permanentes",
            "Luz blanca con 2 destellos cortos",
            "Luz blanca con 1 destello corto y 1 largo",
            "Luz blanca con 3 destellos cortos",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que luces emite una boya cardinal este?",
        options: [
            "Luz blanca con 3 destellos cortos",
            "Luz blanca con destellos cortos permanentes",
            "Luz blanca con 9 destellos cortos",
            "Luz blanca con 2 destellos cortos",
            "Luz blanca con 1 destello corto y 1 largo",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que luces emite una boya cardinal oeste?",
        options: [
            "Luz blanca con 9 destellos cortos",
            "Luz blanca con 3 destellos cortos",
            "Luz blanca con destellos cortos permanentes",
            "Luz blanca con 7 destellos cortos",
            "Luz blanca con 6 destellos cortos y 1 largo",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que luces emite una boya cardinal sur?",
        options: [
            "Luz blanca con 6 destellos cortos y 1 largo",
            "Luz blanca con 9 destellos cortos",
            "Luz blanca con 3 destellos cortos",
            "Luz blanca con destellos cortos permanentes",
            "Luz blanca con 6 destellos cortos",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que luces emite una boya de aguas seguras?",
        options: [
            "Todas las anteriores",
            "Luz blanca isofásica",
            "Luz blanca con ocultaciones",
            "Luz blanca con un destello largo cada 10 segundos",
            "Luz blanca con un destello corto y uno largo (morse A)",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que color de luz emite una boya de marca de babor en America?",
        options: [
            "Verde",
            "Roja",
            "Blanca",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que color de luz emite una boya de marca de estribor en America?",
        options: [
            "Roja",
            "Verde",
            "Blanca",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Que marca de tope tiene una boya de canal de estribor?",
        options: [
            "Cono",
            "Cilindro",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué marca de tope tiene una boya de canal de babor?",
        options: [
            "Cilindro",
            "Cono",
        ],
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya emite luces con este ritmo?",
        media: '<div class="night"><span class="buoy01">.</span></div>',
        options: [
            "Peligro aislado",
            "Aguas seguras",
            "Cardinal norte",
            "Cardinal este",
            "Cardinal oeste",
            "Cardinal sur",
        ],
        explanation: "Dos destellos cortos (seguidos de un descanso).",
        answer: 0
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya emite luces con este ritmo?",
        media: '<div class="night"><span class="buoy05">.</span></div>',
        options: [
            "Peligro aislado",
            "Aguas seguras",
            "Cardinal norte",
            "Cardinal este",
            "Cardinal oeste",
            "Cardinal sur",
        ],
        explanation: "Un destello corto y uno largo (seguidos de un descanso).",
        answer: 1
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya emite luces con este ritmo?",
        media: '<div class="night"><span class="buoy09">.</span></div>',
        options: [
            "Peligro aislado",
            "Aguas seguras",
            "Cardinal norte",
            "Cardinal este",
            "Cardinal oeste",
            "Cardinal sur",
        ],
        explanation: "Destellos cortos permanentes.",
        answer: 2
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya emite luces con este ritmo?",
        media: '<div class="night"><span class="buoy10">.</span></div>',
        options: [
            "Peligro aislado",
            "Aguas seguras",
            "Cardinal norte",
            "Cardinal este",
            "Cardinal oeste",
            "Cardinal sur",
        ],
        explanation: "Tres destellos cortos.",
        answer: 3
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya emite luces con este ritmo?",
        media: '<div class="night"><span class="buoy11">.</span></div>',
        options: [
            "Peligro aislado",
            "Aguas seguras",
            "Cardinal norte",
            "Cardinal este",
            "Cardinal oeste",
            "Cardinal sur",
        ],
        explanation: "Seis destellos cortos y uno largo.",
        answer: 5
    },
    {
        topic: "buoys",
        kind: "text",
        question: "¿Qué boya emite luces con este ritmo?",
        media: '<div class="night"><span class="buoy12">.</span></div>',
        options: [
            "Peligro aislado",
            "Aguas seguras",
            "Cardinal norte",
            "Cardinal este",
            "Cardinal oeste",
            "Cardinal sur",
        ],
        explanation: "Nueve destellos cortos.",
        answer: 4
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QRA?",
        options: [
            "Nombre del barco",
            "Puede comunicarse directamente con...",
            "Quede en espera",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QTH?",
        options: [
            "Posicion",
            "Esta interferida mi transmición",
            "¿Quién llama?",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QRM?",
        options: [
            "Esta interferida mi transmición",
            "Nombre del barco",
            "Quede en espera",
            "Comprendido",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QSO?",
        options: [
            "Puede comunicarse directamente con...",
            "Nombre del barco",
            "¿Tiene algún aviso a los navegantes o aviso de tempestad?",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QRZ?",
        options: [
            "¿Quién llama?",
            "Esta interferida mi transmición",
            "Quede en espera",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QAP?",
        options: [
            "Quede en espera",
            "¿Quién llama?",
            "Comprendido",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QAN?",
        options: [
            "¿Cuál es la dirección y velocidad del viento en la superficie en (lugar)?",
            "¿Cuál es la última observación meteorológica que se dispone para (lugar)?",
            "¿Qué frecuencia de trabajo utilizará usted?",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QAM?",
        options: [
            "¿Cuál es la última observación meteorológica que se dispone para (lugar)?",
            "¿Cuál es la dirección y velocidad del viento en la superficie en (lugar)?",
            "¿Qué frecuencia de trabajo utilizará usted?",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QSS?",
        options: [
            "¿Qué frecuencia de trabajo utilizará usted?",
            "¿Cuál es la última observación meteorológica que se dispone para (lugar)?",
            "¿Cuál es la dirección y velocidad del viento en la superficie en (lugar)?",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QUX?",
        options: [
            "¿Tiene algún aviso a los navegantes o aviso de tempestad?",
            "Posicion",
            "¿Quién llama?",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "Canal de radio para: Avisos, meteorología, PNA y PNN",
        options: [
            "15",
            "16",
            "71",
            "73",
            "78",
        ],
        answer: 0,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "Canal de radio para: Llamadas de emergencia",
        options: [
            "15",
            "16",
            "71",
            "73",
            "78",
        ],
        answer: 1,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "Canal de radio para: Clubes y embarcaciones",
        options: [
            "71 y 78",
            "15",
            "16",
            "71",
            "73",
            "78",
        ],
        answer: 0,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "Canal de radio para: Embarcaciones deportivas",
        options: [
            "15",
            "16",
            "71",
            "73",
            "78",
        ],
        answer: 3,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿Qué canal debe utilizarse para enviar un MAYDAY, PAN PAN o SECURITE?",
        options: [
            "16",
            "15",
            "71",
        ],
        answer: 0,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿Qué mensaje se utiliza para dar un mensaje de socorro o peligro, cuyo significado es que la embarcación o aeronave se encuentra en peligro grave o inminente y solicita auxilio inmediato?",
        options: [
            "MAYDAY",
            "PAN PAN",
            "SECURITE",
        ],
        answer: 0,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿Qué mensaje se utiliza para dar un mensaje relativo a la urgencia del mismo y relacionado con la seguridad de un barco, aeronaves o personas?",
        options: [
            "MAYDAY",
            "PAN PAN",
            "SECURITE",
        ],
        answer: 1,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿Qué mensaje se utiliza para comunicar contenido que tiene que ver con la seguridad de la navegación o que incluye avisos meteorológicos urgentes (como notificación de posición y rumbo con poca visibilidad)?",
        options: [
            "MAYDAY",
            "PAN PAN",
            "SECURITE",
        ],
        answer: 2,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿En el Código Q qué significa QSL?",
        options: [
            "Comprendido",
            "Posicion",
            "Esta interferida mi transmición",
        ],
        answer: 0
    },
    {
        topic: "vhf",
        kind: "text",
        question: "Canal de radio para: Avisos, meteorología, PNA y PNN",
        options: [
            "15",
            "16",
            "71",
            "73",
            "78",
        ],
        answer: 0,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "Canal de radio para: Llamadas de emergencia",
        options: [
            "15",
            "16",
            "71",
            "73",
            "78",
        ],
        answer: 1,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "Canal de radio para: Clubes y embarcaciones",
        options: [
            "71 y 78",
            "15",
            "16",
            "71",
            "73",
            "78",
        ],
        answer: 0,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "Canal de radio para: Embarcaciones deportivas",
        options: [
            "15",
            "16",
            "71",
            "73",
            "78",
        ],
        answer: 3,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿Qué canal debe utilizarse para enviar un MAYDAY, PAN PAN o SECURITE?",
        options: [
            "16",
            "15",
            "71",
        ],
        answer: 0,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿Qué mensaje se utiliza para dar un mensaje de socorro o peligro, cuyo significado es que la embarcación o aeronave se encuentra en peligro grave o inminente y solicita auxilio inmediato?",
        options: [
            "MAYDAY",
            "PAN PAN",
            "SECURITE",
        ],
        answer: 0,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿Qué mensaje se utiliza para dar un mensaje relativo a la urgencia del mismo y relacionado con la seguridad de un barco, aeronaves o personas?",
        options: [
            "MAYDAY",
            "PAN PAN",
            "SECURITE",
        ],
        answer: 1,
    },
    {
        topic: "vhf",
        kind: "text",
        question: "¿Qué mensaje se utiliza para comunicar contenido que tiene que ver con la seguridad de la navegación o que incluye avisos meteorológicos urgentes (como notificación de posición y rumbo con poca visibilidad)?",
        options: [
            "MAYDAY",
            "PAN PAN",
            "SECURITE",
        ],
        answer: 2,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Qué significan las siglas RIPA?",
        options: [
            "Reglamento Internacional para Prevenir Abordajes",
            "Reglamento Intersocial de Puertos Adyacentes",
            "Reglas Instauradas de Puertos y Amarras",
        ],
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso?",
        media: '<img src="images/ripa_buque_que_alcanza.png">',
        options: [
            "A",
            "B",
        ],
        explanation: '<img src="images/ripa_buque_que_alcanza_solucion.png"><p>El buque alcanzado tiene prioridad de paso.</p>',
        answer: 1,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso?",
        media: '<img src="images/ripa_vuelta_encontrada.png">',
        options: [
            "Ambos caen a estribor",
            "Ambos caen a babor",
            "Ambos detienen la marcha",
            "A",
            "B",
        ],
        explanation: '<img src="images/ripa_vuelta_encontrada_solucion.png"><p>Cuando dos buques de propulsión mecánica naveguen con rumbos opuestos o casi opuestos, ambos deben caer a estribor.</p>',
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso?",
        media: '<img src="images/ripa_distintas_amuras.png">',
        options: [
            "A",
            "B",
        ],
        explanation: '<img src="images/ripa_distintas_amuras_solucion.png"><p>Con distintas amuras tiene prioridad de paso el amurado a estribor.</p>',
        answer: 1,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso?",
        media: '<img src="images/ripa_iguales_amuras_2.png">',
        options: [
            "A",
            "B",
        ],
        explanation: '<img src="images/ripa_iguales_amuras_2_solucion.png"><p>Con iguales amuras tiene prioridad de paso el de sotavento.</p>',
        answer: 1,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso?",
        media: '<img src="images/ripa_iguales_amuras.png">',
        options: [
            "A",
            "B",
        ],
        explanation: '<img src="images/ripa_iguales_amuras_solucion.png"><p>Con iguales amuras tiene prioridad de paso el de sotavento.</p>',
        answer: 1,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre 1 y 2?",
        media: '<img src="images/ripa_varios.png">',
        options: [
            "1",
            "2",
            "Ambos caen a estribor",
        ],
        explanation: "2 esta amurado a estribor y 1 a babor",
        answer: 1,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre 1 y 3?",
        media: '<img src="images/ripa_varios.png">',
        options: [
            "1",
            "3",
            "Ambos caen a estribor",
        ],
        explanation: "3 esta amurado a estribor y 1 a babor",
        answer: 1,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre 1 y 4?",
        media: '<img src="images/ripa_varios.png">',
        options: [
            "1",
            "4",
            "Ambos caen a estribor",
        ],
        explanation: "4 esta amurado a babor, 1 a babor y 4 esta a sotavento de 1.",
        answer: 1,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre 2 y 3?",
        media: '<img src="images/ripa_varios.png">',
        options: [
            "2",
            "3",
            "Ambos caen a estribor",
        ],
        explanation: "3 esta amurado a estribor, 2 a estribor y 3 esta a sotavento de 2.",
        answer: 1,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre 2 y 4?",
        media: '<img src="images/ripa_varios.png">',
        options: [
            "2",
            "4",
            "Ambos caen a estribor",
        ],
        explanation: "2 esta amurado a estribor, 4 a babor.",
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre 3 y 4?",
        media: '<img src="images/ripa_varios.png">',
        options: [
            "3",
            "4",
            "Ambos caen a estribor",
        ],
        explanation: "3 esta amurado a estribor, 2 a babor.",
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso?",
        media: '<img src="images/ripa_cruce_motor.png">',
        options: [
            "A",
            "B",
        ],
        explanation: '<img src="images/ripa_cruce_motor_solucion.png"><p>Cuando dos buques de propulsión mecánica se crucen con riesgo de abordaje, tiene prioridad de paso el que este al costado de estribor del otro.</p>',
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre estos dos?",
        options: [
            "Barco a vela",
            "Barco a motor",
        ],
        explanation: "Prioridades:<br>barco a motor < velero<br>velero < buque pesquero<br>buque pesquero < buque con capacidad de maniobra restringida<br>buque con capacidad de maniobra restringida < buque sin gobierno",
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre estos dos?",
        options: [
            "Barco pesquero",
            "Barco a vela",
        ],
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre estos dos?",
        options: [
            "Barco a remo",
            "Barco a vela",
        ],
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre estos dos?",
        options: [
            "Buque sin gobierno",
            "Barco a vela",
        ],
        answer: 0,
    },
    {
        topic: "ripa",
        kind: "text",
        question: "¿Quién tiene prioridad de paso entre estos dos?",
        options: [
            "Buque con capacidad de maniobra restringida",
            "Barco a vela",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Están bien los ángulos y colores de las luces?",
        media: '<img src="images/luces_angulos.png">',
        options: [
            "Si",
            "No",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿A que corresponden estas luces?",
        media: '<img src="images/luces_01.png">',
        options: [
            "Buque a motor menor de 7m",
            "Buque a motor menor de 12m",
            "Buque a vela menor de 20m",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿A que corresponden estas luces?",
        media: '<img src="images/luces_02.png">',
        options: [
            "Buque a motor menor de 12m",
            "Buque a motor menor de 7m",
            "Buque a vela menor de 20m",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿A que corresponden estas luces?",
        media: '<img src="images/luces_03.png">',
        options: [
            "Buque a motor menor de 50m",
            "Buque a motor mayor de 50m",
            "Buque a vela menor de 20m",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿A que corresponden estas luces?",
        media: '<img src="images/luces_04.png">',
        options: [
            "Buque a motor mayor de 50m",
            "Buque a motor menor de 50m",
            "Buque a vela menor de 20m",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿A que corresponden estas luces?",
        media: '<img src="images/luces_05.png">',
        options: [
            "Buque remolcando menor de 50m",
            "Buque remolcando mayor de 50m",
            "Buque a vela mayor de 20m",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál de las siguientes opciones es correcta?",
        media: '<img src="images/luces_06.png">',
        options: [
            "Todas",
            "A",
            "B",
            "C",
            "A y B",
            "B y C",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál corresponde a un buque pesquero de arrastre menor a 50m?",
        media: '<img src="images/luces_07.png">',
        options: [
            "A",
            "B",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál corresponde a un buque pesquero de no arrastre menor a 50m?",
        media: '<img src="images/luces_07.png">',
        options: [
            "A",
            "B",
        ],
        answer: 1,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál corresponde a un buque sin gobierno?",
        media: '<img src="images/luces_08.png">',
        options: [
            "A",
            "B",
            "C",
            "D",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál corresponde a un buque con maniobra restringida?",
        media: '<img src="images/luces_08.png">',
        options: [
            "A",
            "B",
            "C",
            "D",
        ],
        answer: 1,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál corresponde a un buque dragando con restricción de paso?",
        media: '<img src="images/luces_08.png">',
        options: [
            "A",
            "B",
            "C",
            "D",
        ],
        answer: 2,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál corresponde a un buque fondeado menor de 50m?",
        media: '<img src="images/luces_09.png">',
        options: [
            "A",
            "B",
            "C",
            "D",
        ],
        answer: 0,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál corresponde a un buque fondeado mayor de 50m?",
        media: '<img src="images/luces_09.png">',
        options: [
            "A",
            "B",
            "C",
            "D",
        ],
        answer: 1,
    },
    {
        topic: "lights",
        kind: "text",
        question: "¿Cuál corresponde a un buque varado?",
        media: '<img src="images/luces_09.png">',
        options: [
            "A",
            "B",
            "C",
            "D",
        ],
        answer: 3,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué tipo de ancla es mejor para fondos rocosos?",
        options: [
            "Almirantazgo",
            "Danforth",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué tipo de ancla es mejor para fondos blandos?",
        options: [
            "Almirantazgo",
            "Danforth",
        ],
        answer: 1,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué tipo de ancla es esta?",
        media: '<img src="images/anchor_admiralty.jpg">',
        options: [
            "Almirantazgo",
            "Danforth",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué tipo de ancla es esta?",
        media: '<img src="images/anchor_danforth.jpg">',
        options: [
            "Almirantazgo",
            "Danforth",
        ],
        answer: 1,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué peso de ancla tipo Danforth es recomendable para un velero de 20 pies?",
        options: [
            "2Kg",
            "5Kg",
            "8Kg",
        ],
        explanation: "20 pies: 2Kg.<br>30 pies: 5kg.<br>40 pies 8kg.",
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué longitud de cadena es recomendable para fondear con buen tiempo?",
        options: [
            "3 veces la profundidad del lugar",
            "5 veces la profundidad del lugar",
            "7 veces la profundidad del lugar",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué longitud de cadena es recomendable para fondear con mal tiempo?",
        options: [
            "3 veces la profundidad del lugar",
            "5 veces la profundidad del lugar",
            "7 veces la profundidad del lugar",
        ],
        answer: 1,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué longitud de cabo es recomendable para fondear con buen tiempo?",
        options: [
            "3 veces la profundidad del lugar",
            "5 veces la profundidad del lugar",
            "7 veces la profundidad del lugar",
        ],
        answer: 1,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué longitud de cabo es recomendable para fondear con mal tiempo?",
        options: [
            "3 veces la profundidad del lugar",
            "5 veces la profundidad del lugar",
            "7 veces la profundidad del lugar",
        ],
        answer: 2,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Cómo se llama el desplazamiento causado por la corriente?",
        options: [
            "Deriva",
            "Abatimiento",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Cómo se llama el desplazamiento lateral causado por el viento?",
        options: [
            "Abatimiento",
            "Deriva",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué significa que se esta en pleamar?",
        options: [
            "Se está con la marea alta",
            "Se está con la marea baja",
            "Otra cosa",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Cuales de las siguientes son velas de tormenta?",
        options: [
            "Ambas",
            "Vela de capa",
            "Tormentín",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué cabos se utilizan para tomar rizos?",
        options: [
            "Amantes de rizo",
            "Drizas",
            "Escotas",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Cómo se llama al lado por donde viene el viento?",
        options: [
            "Barlovento",
            "Sotavento",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué es orzar?",
        options: [
            "Alterar el rumbo hacia barlovento",
            "Alterar el rumbo hacia sotavento",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué es derivar?",
        options: [
            "Alterar el rumbo hacia sotavento",
            "Alterar el rumbo hacia barlovento",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué es virar?",
        options: [
            "Cambiar de amuras",
            "Alterar el rumbo",
            "Filar una escota",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué es virar por avante?",
        options: [
            "Virar orzando",
            "Virar derivando",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué es virar en redondo?",
        options: [
            "Virar derivando",
            "Virar orzando",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué significa estar amurado a estribor?",
        options: [
            "Que el barco recibe el viento por la banda de estribor",
            "Que el barco recibe el viento por la banda de babor",
            "Otra cosa",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Como nota que el barco esta amurado a estribor?",
        options: [
            "Todas son correctas",
            "El barco recibe el viento por la banda de estribor",
            "El barco tiene la botavara en la banda de babor",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "¿Qué cabo se utiliza para izar las velas?",
        options: [
            "Driza",
            "Escota",
            "Amantillo",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama A?",
        media: '<img src="images/barco02.jpg">',
        options: [
            "Francobordo",
            "Puntal",
            "Calado",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama B?",
        media: '<img src="images/barco02.jpg">',
        options: [
            "Calado",
            "Francobordo",
            "Puntal",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama C?",
        media: '<img src="images/barco02.jpg">',
        options: [
            "Puntal",
            "Calado",
            "Francobordo",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es la obra muerta?",
        media: '<img src="images/barco03.png">',
        options: [
            "A",
            "B",
            "C",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es la obra viva?",
        media: '<img src="images/barco03.png">',
        options: [
            "A",
            "B",
            "C",
        ],
        answer: 1,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es la linea de flotación?",
        media: '<img src="images/barco03.png">',
        options: [
            "A",
            "B",
            "C",
        ],
        answer: 2,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama donde se apoya el palo?",
        options: [
            "Carlinga",
            "Tambucho",
            "Fogonadura",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es el stay proel?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "23",
            "1",
            "22",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es la la pala de timón?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "13",
            "12",
            "16",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es la vela mayor?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "6",
            "25",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es el pujamen de mayor?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "8",
            "3",
            "7",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es el gratil de la mayor?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "7",
            "8",
            "3",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es la baluma de la mayor?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "3",
            "7",
            "8",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es el puño de driza?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "21",
            "20",
            "19",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es el puño de escota?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "9",
            "2",
            "19",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es el puño de amura?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "19",
            "20",
            "21",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es el 14?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Escota de mayor",
            "Escota de foque",
            "Vang",
            "Amantillo",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es la 11?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Escota de foque",
            "Escota de mayor",
            "Vang",
            "Amantillo",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cuál es la botavara?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "10",
            "12",
            "24",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama el 15?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Vang",
            "Escota de mayor",
            "Botavara",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama el 12?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Caña del timón",
            "Pala del timón",
            "Botavara",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama el 25?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Foque",
            "Vela mayor",
            "Casco",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama los 4?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Ambas son correctas",
            "Battens",
            "Sables",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama el 5?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Obenques",
            "Stay popel",
            "Driza",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama el 17?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Casco",
            "Orza",
            "Quillote",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama el 16?",
        media: '<img src="images/barco04.jpg">',
        options: [
            "Orza",
            "Casco",
            "Jarcia",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama 1?",
        media: '<img src="images/barco01.png">',
        options: [
            "Caperol",
            "Roda",
            "Arrufo",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama 2?",
        media: '<img src="images/barco01.png">',
        options: [
            "Roda",
            "Pie de roda",
            "Pantoque",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama 3?",
        media: '<img src="images/barco01.png">',
        options: [
            "Pie de roda",
            "Roda",
            "Arrufo",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama 4?",
        media: '<img src="images/barco01.png">',
        options: [
            "Arrufo",
            "Pie de roda",
            "Pantoque",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama 5?",
        media: '<img src="images/barco01.png">',
        options: [
            "Coronamiento de popa",
            "Caperol",
            "Costado",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama 6?",
        media: '<img src="images/barco01.png">',
        options: [
            "Costado",
            "Pie de roda",
            "Arrufo",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama 7?",
        media: '<img src="images/barco01.png">',
        options: [
            "Pantoque",
            "Roda",
            "Fondo",
        ],
        answer: 0,
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Cómo se llama 8?",
        media: '<img src="images/barco01.png">',
        options: [
            "Fondo",
            "Costado",
            "Caperol",
        ],
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "¿Cuánto dura en promedio un Pampero?",
        options: [
            "45 minutos",
            "15 minutos",
            "2 horas",
            "48 horas",
        ],
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "¿Qué implican isobaras muy juntas en un mapa meteorológico?",
        options: [
            "Que hay mucho viento en la zona por la diferencia de presiones",
            "Que hay poco viento en la zona por la diferencia de presiones",
            "Ninguna de las anteriores",
        ],
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "¿De donde a donde va el aire?",
        options: [
            "De un centro de alta presión a uno de baja",
            "De un centro de baja presión a uno de alta",
        ],
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "¿Qué son las isobaras?",
        options: [
            "Lineas de igual presión en un mapa meteorológico",
            "Crucetas de igual longitud",
        ],
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "¿En que sentido se mueve el aire en un centro de alta presión en el hemisferio sur?",
        options: [
            "Antihorario",
            "Horario",
        ],
        explanation: "<table border='1' align='center'><tr><th>Hemisferio</th><th>Alta presión</th><th>Baja presión</th></tr><tr><td>Norte</td><td>Horario</td><td>Antihorario</td></tr><tr><td>Sur</td><td>Antihorario</td><td>Horario</td></tr></table>",
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "Navegando notamos que la presión indicada por el barómetro disminuyo 50 unidades en 30 minutos. ¿Qué podemos suponer sobre el clima al corto plazo?",
        options: [
            "Se viene una tormenta",
            "Se viene una mejora en el clima",
        ],
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "Fase previa al pasaje del Pampero:<br>Comienza a soplar por horas o días el viento Norte, cálido, la temperatura y la humedad van en sostenido aumento. Es sinónimo hablar de un «prepampero».<br><br>Fase de aproximación y pasaje del Pampero:<br>El tiempo ya está al máximo de temperatura y humedad, el viento norte cambia y proviene desde el sudoeste o el sur.<br>Se ve un frente con nubes cumulonimbus.<br><br>Pasa el Pampero a toda.",
        options: [
            "Correcto",
            "Incorrecto",
        ],
        answer: 0,
    },


    {
        topic: "tides",
        kind: "text",
        question: "¿Durante la luna llena y nueva, en que tipo de mareas se encuentra?",
        options: [
            "Sicigias",
            "Cuadraturas",
        ],
        answer: 0,
    },
    {
        topic: "tides",
        kind: "text",
        question: "¿Durante las sicigias se produce la mayor amplitud de mareas?",
        options: [
            "Verdadero",
            "Falso",
        ],
        answer: 0,
    },
    {
        topic: "tides",
        kind: "text",
        question: "Si la carta náutica indica una profundida de 2 metros y la tabla de mareas de 1 metro ¿Cuál es la profundidad esperada?",
        options: [
            "3 metros",
            "2 metros",
            "1 metros",
        ],
        explanation: "La profundidad de un lugar determinado es igual a la suma del sondaje registrado en la carta para ese lugar y la altura de la marea que se registre en ese instante.",
        answer: 0,
    },
    {
        topic: "tides",
        kind: "text",
        question: "La tablas de mareas no consideran el impacto de los efectos meteorológicos en la altura del agua.",
        options: [
            "Verdadero",
            "Falso",
        ],
        explanation: "Las tablas de mareas se arman con predicciones en base a la posición del Sol y de la Luna respecto a la Tierra, teniendo en cuenta su atracción sobre el agua de los océanos.",
        answer: 0,
    },
    {
        topic: "tides",
        kind: "text",
        question: "¿Cómo se llama a la diferencia entre alturas entre una pleamar y la bajamar siguiente, o viceversa?",
        options: [
            "Amplitud de marea",
            "Altura",
            "Pleamar",
            "Duración de marea",
        ],
        answer: 0,
    },
    {
        topic: "tides",
        kind: "text",
        question: "¿Cómo se llama a la diferencia en horas entre una pleamar y la bajamar siguiente, o viceversa?",
        options: [
            "Duración de marea",
            "Amplitud de marea",
            "Altura",
            "Pleamar",
        ],
        answer: 0,
    },
    {
        topic: "tides",
        kind: "text",
        question: "¿Cuántas pleamares y bajamares hay en el Río de la Plata por día?",
        options: [
            "2",
            "1",
            "4",
        ],
        explanation: "Cuando en el curso de 24 horas se produce una sola pleamar y una sola bajamar, se la denomina marea diurna.<br> Cuando en el mismo periodo se producen dos pleamares y dos bajamares, se la denomina marea semidiurna.<br> La marea mixta es un mix de las anteriores.",
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican los siguientes pitidos: . ?",
        options: [
            "Caigo a estribor",
            "Caigo a babor",
        ],
        explanation: "<table border='1' align='center'><tr><th>Aviso</th><th>Descripción</th></tr><tr><td>.</td><td>Caigo a estribor</td></tr><tr><td>. .</td><td>Caigo a babor</td></tr><tr><td>. . . </td><td>Estoy dando atrás</td></tr><tr><td>. . . . .</td><td>No entiendo su maniobra</td></tr><tr><td>- - .</td><td>Pretendo alcanzarle por su banda de estribor</td></tr><tr><td>- - . .</td><td>Pretendo alcanzarle por su banda de babor</td></tr><tr><td>- . - .</td><td>Conforme con el alcanzamiento</td></tr><tr><td>-</td><td>Buque que navega sin visibilidad y respuesta de otro</td></tr><tr><td>Campanas</td><td>Fondeado en niebla</td></tr><table>",
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican los siguientes pitidos: . . ?",
        options: [
            "Caigo a babor",
            "Caigo a estribor",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican los siguientes pitidos: . . . ?",
        options: [
            "Estoy dando marcha atrás",
            "No entiendo su maniobra",
            "Sigo derecho",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican los siguientes pitidos: . . . . . ?",
        options: [
            "No entiendo su maniobra",
            "Estoy dando marcha atrás",
            "Sigo derecho",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican los siguientes pitidos: - - . ?",
        options: [
            "Pretendo alcanzarle por su banda de estribor",
            "Pretendo alcanzarle por su banda de babor",
            "Conforme con el alcanzamiento",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican los siguientes pitidos: - - . . ?",
        options: [
            "Pretendo alcanzarle por su banda de babor",
            "Pretendo alcanzarle por su banda de estribor",
            "Conforme con el alcanzamiento",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican los siguientes pitidos: - . - . ?",
        options: [
            "Conforme con el alcanzamiento",
            "Pretendo alcanzarle por su banda de estribor",
            "Pretendo alcanzarle por su banda de babor",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican los siguientes pitidos: - ?",
        options: [
            "Buque que navega sin visibilidad / respuesta de otro",
            "Estoy fondeado",
            "Sigo derecho",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: "¿Qué indican repiques de campanas?",
        options: [
            "Estoy fondeado, pero es utilizado también para buque navegando",
            "Estoy fondeado",
            "Buque navegando",
            "Ambas son correctas",
            "Buque navegando, pero es utilizado también para estoy fondeado",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: '¿Cómo indica "conforme con el alcanzamiento"?',
        options: [
            "- . - .",
            "- - . .",
            "- - .",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: '¿Cómo indica "pretendo alcanzarle por su banda de babor"?',
        options: [
            "- - . .",
            "- - .",
            "- . - .",
            ". . . . .",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: '¿Cómo indica "pretendo alcanzarle por su banda de estribor"?',
        options: [
            "- - .",
            "- . - .",
            ". . . . .",
            "- - . .",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: '¿Cómo indica "no entiendo su maniobra"?',
        options: [
            ". . . . .",
            "- - . .",
            "- - .",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: '¿Cómo indica "estoy dando atrás"?',
        options: [
            ". . . ",
            "- - .",
            "- ",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: '¿Cómo indica "caigo a babor"?',
        options: [
            ". . ",
            ".",
            ". . . ",
            "- ",
        ],
        answer: 0,
    },
    {
        topic: "noises",
        kind: "text",
        question: '¿Cómo indica "caigo a estribor"?',
        options: [
            ".",
            ". . ",
            ". . . ",
            "- ",
        ],
        answer: 0,
    },

    {
        topic: "tides2",
        kind: "text",
        question: "La altura de marea se mide sobre el plano de reducción de sondaje.",
        options: [
            "Verdadero",
            "Falso",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "Plano de reduccion de sondaje es una altura arbitraria que es el promedio de las bajamares de una región determinada",
        options: [
            "Verdadero",
            "Falso",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "El nivel medio es",
        options: [
            "El promedio entre pleamares y bajamares para una zona determinada",
            "El promedio entre pleamares para una zona determinada",
            "El promedio entre bajamares para una zona determinada",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "La altura de objetos conocidos en la carta estan medidos desde",
        options: [
            "el nivel medio",
            "el plano de reducción de sondaje",
            "el fondo",
            "el agua",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "Usted debe pasar con su buque por una zona de poca profundidad (sondaje de la carta 5.3 metros). El margen de seguridad debajo del casco para el pasaje por el lugar es de 2 metros y su buque cala 4,8 metros. La altura de marea mínima que debe haber para pasar por dicho lugar es de:",
        options: [
            "1,5 metros",
            "3,8 metros",
            "2,5 metros",
            "Puede pasar en todo momento",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "El sondaje es:",
        options: [
            "la distancia vertical desde el plano de reducción de sondaje hasta el fondo",
            "la distancia vertical desde el nivel medio hasta el fondo",
            "la distancia vertical desde la embarcación hasta el fondo",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "La carta naútica muestra",
        options: [
            "el plano de reducción de sondaje",
            "la profundidad",
            "el nivel medio",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "La profundidad es la altura de marea + sondaje",
        options: [
            "Verdadero",
            "Falso",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "El pronóstico mareológico es:",
        options: [
            "el pronostico de mareas corregido por los efectos metereológicos del día",
            "el pronostico de la altura de las horas del día",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "En su navegación de entrada a un puerto, deberá pasar por debajo de un puente cuya altura sobre el nivel medio es de H = 50 metros. De la tabla de mareas obtiene que el plano de reducción de sondaje pasa 2,99 metros por debajo del nivel medio del mar. Su buque tiene una altura máxima medida desde la quilla a la parte mas alta del mismo de 52 metros. El calado actual es de 6,3 metros y el espacio libre que debe dejar entre la parte inferior del puente y la parte mas elevada del buque es de 2 metros. La carta indica una profundidad debajo del puente de 4,2 metros. La altura de marea máxima que le permitirá pasar por debajo del puente es de:",
        options: [
            "5,29 metros",
            "9,49 metros",
            "0,99 metros",
            "No es posible pasar debajo del puente",
        ],
        answer: 0,
        explanation: '<img src="images/caladoaereo.png"> <hr> <p> altura minima:<br> agua minima necesitada = 6.3m (calado)<br> agua minima - plano de reducción de sondaje = 6.3m - 4.2m = 1.1m </p> <p> altura maxima:<br> elevacion + delta nivel medio y plano de reducción de sondaje - altura marea<br> >= margen + altura maxima - calado </p> <p> 50m + 2.99m - h >= 2m + 52m - 6.3m<br> 50m + 2.99m - 2m - 52m + 6.3m >= h<br> h <= 5.29m </p> <p> alternativamente: calado aereo = altura maxima - calado </p> <hr> <p> aire entre el techo del puente y la obra viva =<br> altura máxima - calado + margen = 52m - 6.3m + 2m = 47.7m </p> <p> altura total =<br> altura puente + delta nivel medio y plano de reducción de sondaje + sondaje =<br> 50m + 2.99m + 4.2m = 57.19m </p> <p> espacio de sobra = altura total - altura maxima = 57.19m - 52m = 5.19m<br> agua debajo de sobra = espacio de sobra - margen superior = 5.19m - 2m = 3.19m<br> </p> <p> agua total = agua debajo de sobra + calado  = 3.19m + 6.3m = 9.49m </p> <p> altura de marea máxima = agua total necesaria - plano de reducción de sondaje =<br> 9.49m - 4.2m = 5.29m </p>',
    },
    {
        topic: "tides2",
        kind: "text",
        question: "Para aplicar la regla de los duodécimos:",
        options: [
            "La duración se divide por 6 y la amplitud se divide por 12",
            "La duración se divide por 12 y la amplitud se divide por 12",
            "La duración se divide por 12 y la amplitud se divide por 6",
            "La duración se divide por 6 y la amplitud se divide por 6",
        ],
        answer: 0,
        explanation: "Se divide en seis períodos de 1/12, 2/12, 3/12, 3/12, 2/12, 1/12.",
    },
    {
        topic: "tides2",
        kind: "text",
        question: "Edad de la marea:",
        options: [
            "El retraso entre el paso de la luna nueva o llena por un determinado lugar y la respuesta física en la marea provocada por esa luna, expresado en días y horas",
            "El tiempo promedio entre las pleamares y las bajamares de sicigia, obtenida durante una lunación completa",
            "El tiempo que transcurre entre las mareas de sicigia equinocciales y las mareas de sicigia tropicales",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "Establecimiento de puerto vulgar:",
        options: [
            "El intervalo promedio obtenido durante una lunacion completa, entre el pasaje de la luna nueva o llena por el meridiano del lugar y la ocurrencia de la proxima pleamar",
            "El intervalo promedio obtenido durante una lunacion completa, entre el pasaje de la luna nueva por el meridiano del lugar y la ocurrencia de la aproxima bajamar",
            "El intervalo promedio obtenido durante una lunación completa, entre el pasaje de cualquier luna por el meridiano del lugar y la ocurrencia de la aproxima bajamar",
            "El intervalo promedio obtenido durante una lunacion completa, entre el pasaje de cualquier luna por el meridiano del lugar y la ocurrencia de la aproxima pleamar",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "Establecimiento de puerto Medio:",
        options: [
            "El Intervalo promedio obtenido durante una lunación completa, entre el pasaje de cualquier luna por el meridiano del lugar y la ocurrencia de la próxima pleamar",
            "El Intervalo promedio obtenido durante una lunación completa, entre el pasaje de la luna nueva por el meridiano del lugar y la ocurrencia de la próxima bajamar",
            "El Intervalo promedio obtenido durante una lunación completa, entre el pasaje de cualquier luna por el meridiano del lugar y la ocurrencia de la próxima bajamar",
            "El Intervalo promedio obtenido durante una lunación completa, entre el pasaje de la luna llena o nueva por el meridiano del lugar y la ocurrencia de la próxima pleamar",
        ],
        answer: 0,
    },
    {
        topic: "tides2",
        kind: "text",
        question: "Para calculo de ejercicios de mareas:",
        options: [
            "Todas son correctas",
            "Se despeja una incognita de DIAC: duración, intervalo, amplitud, correción.",
            "Intervalo = tiempo entre la el comienzo de la plea/baja y el momento de interes.",
            "Correción = diferencia de altura entre la altura de la plea/baja y el momento de interes.",
        ],
        answer: 0,
        explanation: '<img src="images/mareas0.png">',
    },
    {
        topic: "tides2",
        kind: "text",
        question: "Siendo elípticas las órbitas de la Luna y el Sol la distancia de estos astros respecto de la Tierra varía con la época del año, dando en ciertas fechas las pleamares y bajamares muy acentuadas:",
        options: [
            "Todo es correcto (salvo que es inventado)",
            "Cuando la distancia de la Luna es mínima, se producen lasmareas de perigeo",
            "Cuando la distancia de la Luna es máxima, se producen lasmareas de apogeo",
            "Todo es inventado",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "",
        options: [
            "Admision, compresion, explosion y escape",
            "Admision, explosion, escape",
            "Explosion, escape, enfriamiento",
            "Enfriamiento, admision, explosion, espape",
        ],
        answer: 0,
    },
    {
        topic: "course",
        kind: "text",
        question: "El abatimiento es",
        options: [
            "El efecto de viento sobre la embarcacion",
            "El efecto de la corriente sobre la obra viva",
            "El efecto conjunto de la corriente y el viento sobre el casco",
            "Un fenomeno meteorologico",
        ],
        answer: 0,
    },
    {
        topic: "course",
        kind: "text",
        question: "el abatimiento",
        options: [
            "Es mayor en ceñida",
            "Es mayor cuando mas a popa ingresa el viento",
            "Es mayor al traves",
            "Es menor en ceñida",
        ],
        answer: 0,
        explanation: "Por la descomposición vectorial del viento sobre las velas y porque la quilla ofrece menos resistencia frente al agua por la posición y el escoramiento.",
    },
    {
        topic: "course",
        kind: "text",
        question: "La deriva",
        options: [
            "Es el efecto de la corriente sobre el casco",
            "Es el efecto del viento sobre el casco",
            "Depende de la obra muerta",
            "Es mayor en un buque de gran porte que en un velero pequeño",
        ],
        answer: 0,
    },
    {
        topic: "course",
        kind: "text",
        question: "La derrota",
        options: [
            "Es la resultante de el efecto del abatimiento y la deriva sobre el rumbo",
            "Se obtiene de la tabla de mareas",
            "No se puede calcular",
            "Se mide en nudos",
        ],
        answer: 0,
    },
    {
        topic: "charts",
        kind: "text",
        question: "El alcance geografico de un faro depende",
        options: [
            "Tanto de la elevacion del ojo como la del faro",
            "Solo de la altura del faro",
            "Sola de la elevacion del ojo",
            "De la luz del faro",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "Fondeo a barba de gato consiste en",
        options: [
            "Fondear con dos anclas por proa",
            "Fondear con un ancla en proa",
            "Fondear por proa y por popa",
            "En engalgar los fondeos",
        ],
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "En la escala beaufort, el grado 5 corresponde a vientos de:",
        options: [
            "17-21 nudos",
            "1-3 nudos",
            "7-10 nudos",
            "34-40 nudos",
        ],
        answer: 0,
        explanation: '<img src="images/beaufort.png">',
    },
    {
        topic: "charts",
        kind: "text",
        question: "La loxodromia",
        options: [
            "Es asintotica respecto a los polos y corta a los meridianos en el mismo angulo",
            "Es asintotica respecto a los polos",
            "En la proyeccion mercator se representa como una curva",
            "Corta a los meridianos en el mismo angulo",
        ],
        answer: 0,
    },
    {
        topic: "charts",
        kind: "text",
        question: "La ortodromia",
        options: [
            "Todas son correctas",
            "Es la distancia mas corta entre dos puntos en la tierra",
            "Se cambia el rumbo continuamente",
            "Se logra al navegar sobre un circulo maximo",
        ],
        answer: 0,
        explanation: "orto: recto, dromia: camino",
    },
    {
        topic: "general",
        kind: "text",
        question: "Ante incendio del tablero electrico. ¿que es lo primero que debo hacer?",
        options: [
            "Desconectar baterias",
            "Tirar agua",
            "Usar un extintor de tipo a",
            "Usar un extintor de tipo b",
            "Usar un extintor de tipo c",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "Poner el barco a la capa consiste en:",
        options: [
            "Acuartelar vela de proa con la mayor a crujia y llevar el timon a la orza",
            "Llevar el timon a la orza y filar mayor",
            "Bajar las velas",
            "Poner el ancla de mar",
        ],
        answer: 0,
    },
    {
        topic: "general",
        kind: "text",
        question: "La maniobra del hombre al agua se realiza:",
        options: [
            "Colocandonos en un traves, avanzar 5 esloras, virar por avante y avanzar hasta ver a la persona entre los obenques, estar a una distancia de 1 eslora o 2 mangas, saltar el foque al estar la persona a la altura de la proa, ahi orzar por completo hacia la persona, filar mayor, dejar el barco entre el viento y la persona parado",
            "De otra forma",
        ],
        answer: 0,
    },
    {
        topic: "meteorology",
        kind: "text",
        question: "¿La brisa de mar se produce porque?",
        options: [
            "La tierra esta mas caliente que el agua",
            "El agua esta mas caliente que la tierra",
            "El agua y la tierra estan a la misma misma temperatura",
            "Hay mucha diferencia barometrica",
        ],
        answer: 0,
        explanation: "La tierra se calienta, se calienta el aire sobre ella que se eleva bajando la presión, el aire sobre el mar con menor temperatura y mayor presión va hacia la tierra.",
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Como se llama al cabo de amarre 1?",
        media: '<img src="images/nombre_cabos_amarra.png">',
        options: [
            "Sping",
            "Travesin",
            "Largo",
            "Corto",
            "Backslash",
        ],
        answer: 0
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Como se llama al cabo de amarre 2?",
        media: '<img src="images/nombre_cabos_amarra.png">',
        options: [
            "Travesin",
            "Sping",
            "Largo",
            "Corto",
            "Backslash",
        ],
        answer: 0
    },
    {
        topic: "boats",
        kind: "text",
        question: "¿Como se llama al cabo de amarre 3?",
        media: '<img src="images/nombre_cabos_amarra.png">',
        options: [
            "Largo",
            "Sping",
            "Travesin",
            "Corto",
            "Backslash",
        ],
        answer: 0
    },
]
