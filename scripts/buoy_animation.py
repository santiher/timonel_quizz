from math import floor


template = """
.buoy%(code)s {
  animation: blink%(code)s %(duration).2fs linear infinite;
}

@keyframes blink%(code)s {
  %(css)s
}
"""


def buoy_animation(
        blinks,
        code_name,
        short_duration=0.2,
        long_duration=0.8,
        off_duration=0.6,
        end_delay=2.0):
    """Generates a CSS animation for blinking lights.

    Parameters
    ----------
    blinks: str
        A string conformed of {s, l} specifying short / long blinks succesions.
    code_name: str
        Code name for the css animation.
    short_duration: float
        Duration of a short blink in seconds.
    long_duration: float
        Duration of a long blink in seconds.
    off_duration: float
        Duration of the off lights interval between blinks in seconds.
    end_delay: float
        Seconds until the animation starts again.

    Returns
    -------
    str: a string containing the css animation
    """
    # Definitions
    blink_duration = {'s': short_duration, 'l': long_duration}
    step = '%s%% {opacity: %d;}'
    total_duration = (
        sum(blink_duration[blink_kind] for blink_kind in blinks)
        + off_duration * len(blinks)
        + end_delay)
    percentage = {
        's': floor(100 * short_duration / total_duration),
        'l': floor(100 * long_duration / total_duration),
        'o': floor(100 * off_duration / total_duration)}
    # Initialization
    steps = []
    current_percentage = 0
    # Steps
    for blink_kind in blinks:
        # Turn lights off
        steps.append(step % (current_percentage, 0))
        # Keep lights off
        steps.append(step % (current_percentage + 1, 0))
        # Finish lights off
        current_percentage += percentage['o']
        steps.append(step % (current_percentage - 1, 0))
        # Turn lights on
        steps.append(step % (current_percentage, 100))
        # Keep lights on
        steps.append(step % (current_percentage + 1, 100))
        # Finish lights on
        current_percentage += percentage[blink_kind]
        steps.append(step % (current_percentage - 1, 100))
    # Turn lights off: end delay
    steps.append(step % (current_percentage, 0))
    # Keep lights off: end delay
    steps.append(step % (100, 0))
    css = '\n  '.join(steps)
    data = {'code': code_name, 'duration': total_duration, 'css': css}
    return template % data


def all_buoys():
    buoys = [
        buoy_animation('ss', '01'),  # peligro_aislado
        buoy_animation('sl', '05'),  # aguas_seguras
        buoy_animation('s', '09', end_delay=0),  # cardinal_norte
        buoy_animation('sss', '10'),  # cardinal_este
        buoy_animation('ssssssl', '11'),  # cardinal_sur
        buoy_animation('sssssssss', '12'),  # cardinal_oeste
        ]
    return ''.join(buoys)


print(all_buoys())
